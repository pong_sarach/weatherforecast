import Link from 'next/link'
import './page.css';

export default function Home() {
    return (
        <div className='center-screen gradient-bg'>
            <div>
                <Link href="/detail">
                    <button className='button'>Welcome to the weather forecast project</button>
                </Link>
            </div>
        </div>
    )
}
