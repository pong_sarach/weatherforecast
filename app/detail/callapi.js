'use client'
import React, { Component } from "react";
import './callapi.css';
import weatherCode from './weather_code.json';
import { Chart } from "react-google-charts";
import { Sunrise, Sunset, Thermometer } from 'react-feather';
 
export default class CallAPI extends Component {
    constructor(props){
        super(props)
        this.state = {
            fetchedData: {
                daily: {sunrise: ["-"], sunset: ["-"], temperature_2m_max:[], temperature_2m_min:[], weather_code:[]},
                hourly: {temperature_2m:[], time:[]}
            },
            hourlyTemp: [
                ["hours", "temp"]
            ]
        }
    }

    componentDidMount(){
        this.fetchData();
    }
    
    //Get data from api server
    fetchData = () => {
        fetch('https://api.open-meteo.com/v1/forecast?latitude=13.754&longitude=100.5014&hourly=temperature_2m&daily=weather_code,temperature_2m_max,temperature_2m_min,sunrise,sunset,precipitation_sum&timezone=Asia%2FBangkok')
           .then((response) => response.json())
           .then((data) => {
                // console.log(data)
                this.setState({
                    fetchedData: data
                })
           })
           .catch((err) => {
              console.log(err.message);
           }
        )
    }

    currentTemp = () => {
        let date = new Date()
        let currentHour = date.getHours()
        let currentDate = date.getDate()
        
        let currentDateTime = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear()
        let currentTemperature = 0

        var hourlyTempData = [["hour","temp"]]

        this.state.fetchedData.hourly.time.map((data, index) => {
            if(data.substring(11,13) == currentHour && data.substring(8,10) == currentDate){
                currentTemperature = this.state.fetchedData.hourly.temperature_2m[index]
            }

            if(data.substring(8,10) == currentDate){
                hourlyTempData.push([data.substring(11,13),this.state.fetchedData.hourly.temperature_2m[index]])
            }
        })

        return(
            <div style={{textAlign: 'center'}}>
                <h1>{currentTemperature}°C</h1>
                <h5>{currentDateTime}</h5>
            </div>
        )
    }

    currentIcon = () => {
        let date = new Date()
        let currentHour = date.getHours()

        let description = ""
        let imageUrl = ""
        Object.entries(weatherCode).forEach(data => {
            if(data[0] == this.state.fetchedData.daily.weather_code[0]){
                
                if(6 <= currentHour <= 18){
                    description = data[1].day.description
                    imageUrl = data[1].day.image
                }else{
                    description = data[1].night.description
                    imageUrl = data[1].night.image
                }
            }
        })
        return(
            <div>
                <div style={{alignItems: "center", justifyContent: "center", display: "flex"}}>
                    <img style={{width:"20%"}} src={imageUrl} alt="weatherImage" />
                </div>
                <h2 style={{textAlign: "center"}}>{description}</h2>
            </div>
        )
    }

    hourlychart = () => {
        let date = new Date()
        let currentDate = date.getDate()

        var hourlyTempData = [["hour","temp"]]

        this.state.fetchedData.hourly.time.map((data, index) => {
            if(data.substring(8,10) == currentDate){
                hourlyTempData.push([data.substring(11,16),this.state.fetchedData.hourly.temperature_2m[index]])
            }
        })

        return(
            <Chart
                chartType="LineChart"
                loader={<div>Loading Chart</div>}
                data={hourlyTempData}
                options={{
                    title: "Hourly Temperature",
                    hAxis: { title: "Time", textPosition: 'none'},
                    vAxis: { title: "Temp (°C)", viewWindow: { min: 20, max: 45 } },
                    legend: "none"
                    }}
                width="100%"
                height="400px"
                legendToggle
            />
        )
    }

    render() {
        // console.log(this.state)
        return(
            <div style={{height:'100vh'}}>
                <div className="page_header">Weather Forecast</div>
                <div className="textCenter">{this.state.fetchedData.timezone}</div>

                {this.currentIcon()}

                <div className="row" style={{alignItems: "center", justifyContent: "center"}}>
                    <div className="cardbox">
                        {this.currentTemp()}
                    </div>
                </div>
                
                <div className="row" style={{alignItems: "center", justifyContent: "center"}}>
                    <div className="cardbox">
                        <div className="row" style={{padding: "10px"}}>
                            <Thermometer/>
                            <h5>Min: </h5>
                            <div>{this.state.fetchedData.daily.temperature_2m_min[0]}°C</div>
                        </div>
                        <div className="row" style={{padding: "10px"}}>
                            <Thermometer/>
                            <h5>Max: </h5>
                            <div>{this.state.fetchedData.daily.temperature_2m_max[0]}°C</div>
                        </div>
                    </div>
                  
                    <div className="cardbox">
                        <div className="row" style={{padding: "10px"}}>
                            <Sunrise />
                            <div style={{paddingLeft: "4px"}}>{this.state.fetchedData.daily.sunrise[0].substring(11)}</div>
                        </div>
                        <div className="row" style={{padding: "10px"}}>
                            <Sunset />
                            <div style={{paddingLeft: "4px"}}>{this.state.fetchedData.daily.sunset[0].substring(11)}</div>
                        </div>
                    </div>
                </div>

                {this.hourlychart()}
            </div>
        )
    }
}